import numpy as np
import matplotlib.pyplot as plt

Vrms = 220 #volt
w0 = 50 #hz
R = 15 #ohm 
L = 5 #henry

t = np.linspace(2.0115,2.3,1000)
#I adjust t0=2.0115 bcuz voltage phase angle is close to 0
#adjust t to 0,1.0 for see transient
#adjust t more than 1.0 for see steady state,recommend t>2.0

A = 4.4/(1+((R*R)/(2500*L*L))) #A,B,C by Partial fraction
B = -A/L
C = R*A

termexpo = (A/L)*np.exp(-(R/L)*t) #I got this 3 by Laplace solution
termcos = B*np.cos(w0*t)
termsin = (C/50)*np.sin(w0*t)

currentI = termexpo+termcos+termsin
voltsource = Vrms*np.sqrt(2)*np.sin(w0*t)
power = voltsource*currentI

#plt.plot(t,currentI) I not use bcuz this graph too small
plt.plot(t,currentI*100) #*100 for easy to compare V-I or P-V-I graph
plt.plot(t,voltsource)
plt.plot(t,power)
plt.grid()
plt.show()

