# RL serie - Plot result
* VI transient
![VItransient](/uploads/5d45f03116a8754cd6fd3e9cea72c92d/VItransient.png)
* P transient
![Ptransient](/uploads/3de943dd7a6fd6ae191750ac09ea6ef2/Ptransient.png)
* PVI transient
![PVItransient](/uploads/7cf9cfd4a43e749e481920111ecfe1bf/PVItransient.png)
* PVI steady state
![PVIsteadystate](/uploads/7cb11f9d11a6fc3baa70b09dbb8e5d0e/PVIsteadystate.png)

Current funtion in time domain by Laplace transform
![image](/uploads/4503db06c19b21edf0890b8e1e03825d/image.png)

## keyword 
* transient
* RL serie circuit
* AC component DC component
* Laplace transform

## Related Links
* https://phyblas.hinaboshi.com/umaki19 //สอนวาดกราฟต่างๆ
* https://phyblas.hinaboshi.com/saraban/numpy_matplotlib //สอนวาดกราฟต่างๆ
* https://raspberrypi-thailand.blogspot.com/2017/09/raspberry-pi-3.html //สอนวาดกราฟ
* gitlab markdown : https://docs.gitlab.com/ee/user/markdown.html //ภาพรวมgitlab markdown 
* gitlab markdown upload image : https://www.youtube.com/watch?v=nvPOUdz5PL4 //gitlab markdown อัพรูปลงในreadme
