# คำที่ควรรู้ (ชุดที่ 1)
Repository, Clone, Commit, Unstaged, Staged, Push, Pull, Fetch, Conflict, Merge Commit ปล.อ่านแบบละเอียดได้ที่ลิ้งแรก
* Repository : การสร้างโฟลเดอร์เพื่อสร้างโปรเจค
* Clone : การดึงRepositorจากgitมาลงในเครื่อง
* Commit : การback upโค้ดที่แก้เสร็จแล้วลงgit โดยในideจะแสดงโค้ดสีแดง(ก่อนแก้) และสีเขียว(หลังแก้) ให้ดูด้วย
![Commit](/uploads/76d9683a0a011b96cfcc94f6020c8224/Commit.png)
* Unstage & stage : Unstageคือไฟล์ที่อยุ่ในสถานะ กำลังแก้ไข ส่วนstageก็คือ เสร็จแล้ว commitได้
* Push : การส่ง commit ขึ้น git
* Pull : การนำ commit จาก git ลงเครื่องเรา
* Fetch : การเช็คว่ามีใคร Push commit อะไรขึ้นไปใน git มั่ง
* Merge Commit : การแก้โค้ดที่เกิดจากการที่เราCommitหลังคนอื่น > เราต้องเอาโค้ดนั้นมารวมกับของเรา
* Conflict : การที่รวมโค้ดแล้ว มันมีส่วนที่มันเข้ากันไม่ได้ 

![สรุปgitชุดแรก](/uploads/4bcf86966039b9da9f6d4f9ec5113dc1/สรุปgitชุดแรก.png)

# คำที่ควรรู้ (ชุดที่2)
Branch, Merge Branch, Stash, Unstash, Git Flow, Tag, Fork, Pull Request
* Branch : มันคือHistoryของCommit 
![branch](/uploads/afcf493e4af65aeafa00a98956b2782d/branch.png)
* Merge Branch : การรวม branch เนื่องจากการพัฒนาโปรแกรมคนละส่วนกัน
* Stash & Unstash : Stashเหมือนกล่องสำหรับเก็บโค้ดที่ยังไม่เสร็จ การเอาโค้ดออกมาเรียกว่าUnstash
* Gitflow : ระเบียบในการแตกbranch แบ่งbranchออกเป็น 5 ประเภท > master,develop,feature,release,hotfix
![ประเภทของbranch](/uploads/2a5f360b2ce0ad99731e0c831a764bb1/ประเภทของbranch.png)
* Tag : เหมือนป้ายสินค้า แขวนไว้ที่ branch master ซึ่งมันมักจะเป็นVersionของโปรแกรมนั้น ใช้เพื่อให้หาcommitได้ง่าย
* Fork : คัดลอก Repository (ของคนอื่นที่อนุญาตให้เราเข้าไปดูได้) มาเก็บไว้เป็น Repository ของเรา
* Pull request : การขออนุญาติpull commitของเราเข้าไปที่Repositoryคนอื่น







## ลิ้งที่เกี่ยวข้อง
* ศัพท์ต่างๆ ที่ควรรู้ : https://medium.com/20scoops-cnx/github-workflow-from-scratch-99b07e8c318b
* ขั้นตอนการทำ work flow : https://medium.com/20scoops-cnx/github-workflow-from-scratch-99b07e8c318b
* 